import queryString from "query-string";

export default {
    state: {
        products: [],
        empty: true,
        subtotal: null,
        total: null,
        changed: false,
        shipping: null
    },

    getters: {
        products(state) {
            return state.products;
        },

        count(state) {
            return state.products.length;
        },

        empty(state) {
            return state.empty;
        },

        subtotal(state) {
            return state.subtotal;
        },

        total(state) {
            return state.total;
        },

        changed(state) {
            return state.changed;
        },

        shipping(state) {
            return state.shipping;
        }
    },

    mutations: {
        SET_PRODUCTS(state, products) {
            state.products = products;
        },

        SET_EMPTY(state, empty) {
            state.empty = empty;
        },
        SET_SUBTOTAL(state, subtotal) {
            state.subtotal = subtotal;
        },
        SET_TOTAL(state, total) {
            state.total = total;
        },

        SET_CHANGED(state, changed) {
            state.changed = changed;
        },
        SET_SHIPPING(state, shipping) {
            state.shipping = shipping;
        }
    },

    actions: {
        async getCart({ commit, state }) {
            let query = {};

            let response = await axios.get(
                `cart?${queryString.stringify(query)}`
            );

            commit("SET_PRODUCTS", response.data.products);
            commit("SET_EMPTY", response.meta.empty);
            commit("SET_SUBTOTAL", response.meta.subtotal);
            commit("SET_TOTAL", response.meta.total);
            commit("SET_CHANGED", response.meta.changed);
            return response;
        },

        async destroy({ dispatch }, productId) {
            let response = await axios.delete(`cart/${productId}`);

            dispatch("getCart");
        },

        async update({ dispatch }, { productId, quantity }) {
            let response = await axios.patch(`cart/${productId}`, {
                quantity
            });

            dispatch("getCart");
        },

        async store({ dispatch }, products) {
            let response = await axios.post("cart", {
                products
            });

            dispatch("getCart");
        }
    }
};
