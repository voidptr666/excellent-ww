/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue").default;
import Vuex from "vuex";
import VueRouter from "vue-router";
import storeData from "./store/index";
import Home from "./views/index";
import SingleProduct from "./views/products/_id";

import LoginComponent from "./components/LoginComponent";

Vue.use(VueRouter);
const store = new Vuex.Store(storeData);

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/login",
            name: "login-component",
            component: LoginComponent
        },
        {
            path: "/",
            name: "product-component",
            component: Home
        },
        {
            path: "/product/:id",
            name: "SingleProduct",
            component: SingleProduct
        }
    ]
});
const app = new Vue({
    el: "#app",
    store,
    router
});
