<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;

\
Route::resource('products', ProductController::class);
Route::resource('orders', OrderController::class);

Route::group(['prefix' => 'auth'], function () {

    Route::post('register', RegisterController::class);
    Route::post('login', [LoginController::class, 'login']);
    //Route::get('profile', 'ProfileController@action');
});


Route::resource('cart', 'Cart\CartController', [
    'parameters' => [
        'cart' => 'productVariation'
    ]
]);
