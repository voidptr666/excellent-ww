<?php

namespace App\Http\Controllers\Cart;

use App\Http\Controllers\Controller;
use App\Http\Requests\CartAddRequest;
use Illuminate\Http\Request;
use App\Cart\Cart;

use App\Http\Requests\CartUpdateRequest;
use App\Http\Resources\CartResource;
use App\Models\Product;

class CartController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth:api']);
    }

    public function index(Request $request, Cart $cart)
    {
        $cart->sync();

        $request->user()->load(['cart.product']);

        return (new CartResource($request->user()))
            ->additional([
                'meta' => $this->meta($cart, $request)
            ]);
    }

    protected function meta(Cart $cart, Request $request)
    {
        return [
            'empty' => $cart->isEmpty(),
            'subtotal' => $cart->subtotal()->formatted(),
            'total' =>  $cart->total()->formatted(),
            'changed' => $cart->hasChanged(),
        ];
    }

    public function store(CartAddRequest $request, Cart $cart)
    {

        // $products = $request->products;

        // $products = collect($products)->keyBy('id')->map(function($product){
        //     return [
        //         'quantity' => $product['quantity']
        //     ];
        // })->toArray();


        // $request->user()->cart()->syncWithoutDetaching($products);

        $cart->add($request->products);
    }

    public function update(Product $product, CartUpdateRequest $request, Cart $cart)
    {
        //dd($product);

        $cart->update($product->id, $request->quantity);
    }

    public function destroy(Product $id, Cart $cart)
    {
        //dd($id);

        $cart->delete($id->id);
    }
}
