<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ProductResource;
use App\Models\Product;

class ProductController extends Controller
{
    //

    public function index()
    {
        $products = Product::with(['stock'])->paginate(9);
        return ProductResource::collection($products);
    }
}
