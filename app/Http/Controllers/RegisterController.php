<?php

namespace App\Http\Controllers;

use App\Http\Resources\AuthUserResource;
use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    //

    public function __invoke(Request $request)
    {
        $user = User::create($request->only('email', 'name', 'password'));

        return new AuthUserResource($user);
    }
}
