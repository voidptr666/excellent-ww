<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\OrderResource;
use App\Cart;

class OrderController extends Controller
{
    //
    public function index()
    {
        $orders = request()->user()->orders()->with(['products']);

        return new OrderResource($orders);
    }


    public function store(Request $request, Cart $cart)
    {
        # code...

        if ($cart->isEmpty()) {
            return response(null, 400);
        }

        $order = $this->createOrder($request, $cart);

        $order->products()->sync($cart->products()->forSyncing());

        //event(new OrderCreated($order));

        return new OrderResource($order);
    }

    protected function createOrder(Request $request, Cart $cart)
    {
        return $request->user()->orders()->create(
            array_merge($request->all(), [
                'subtotal' => $cart->subtotal()->amount()
            ])
        );
    }
}
