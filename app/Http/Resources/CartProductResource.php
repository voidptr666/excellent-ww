<?php

namespace App\Http\Resources\Cart;

use App\Cart\Money;
use App\Http\Resources\ProductIndexResource;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CartProductResource extends ProductResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'product' => new ProductResource($this->product),
            'quantity' => $this->pivot->quantity,
            'total' => $this->getCartTotal()->formatted()
        ]);
    }

    protected function getCartTotal()
    {
        return new Money($this->pivot->quantity * $this->price->amount());
    }
}
