<?php

namespace App\Models;

use App\Cart\Money;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = [
        'subtotal',
        'unique_order_no',
        'prepared_at',
        'delivery_date'
    ];

    public function getSubtotalAttribute($subtotal)
    {
        return new Money($subtotal);
    }

    public function total()
    {
        return $this->subtotal;
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_order')->withPivot(['quantity'])->withTimestamps();
    }
}
