<?php

namespace App\Models;

use App\Cart\Money;
use App\Models\Collections\ProductCollection;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\HasPrice;


class Product extends Model
{
    //
    use HasPrice;

    public function getPriceAttribute($value)
    {
        if ($value === null) {
            return $this->product->price;
        }
        return new Money($value);
    }

    public function minStock($count)
    {
        return min($this->stockCount(), $count); //200
    }


    public function priceVaries()
    {
        return $this->price->amount() !== $this->product->price->amount();
    }

    public function inStock()
    {
        return $this->stockCount() > 0;
    }

    public function stockCount()
    {
        return $this->qty;
    }

    public function newCollection(array $models = [])
    {
        # code...
        return new ProductCollection($models);
    }
}
