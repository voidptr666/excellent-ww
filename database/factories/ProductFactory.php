<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'title' => Arr::random(['Oranges for 50% off', 'Potatoes for cheap!!!', 'Onions never cheap!']),

            'image' => '/images/1.png',
            'price' => 30000,
            'qty' => 4
        ];
    }
}
